const { Router } = require('express');

const UserController = require('./app/controllers/UserController');
const SessionController = require('./app/controllers/SessionController');

routes = new Router();

routes.post('/users', UserController.store);
routes.post('/sessions', SessionController.store);

module.exports = routes;
