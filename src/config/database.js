module.exports = {
  dialect : 'postgres',
  host: 'localhost',
  username: 'catrix',
  password: 'admin',
  database: 'postgres',
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true,
  },
};
